const efs = require('./index');
const express = require('express');
const http = require('http');

const app = express();

efs({ app });

const server = http.createServer(app);

server.listen(3000);