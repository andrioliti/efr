const path = require('path');
const fs = require('fs');

const DEFAULT_ROUTER_PATH = '/router';

const removeExtension = (p) => p.split('.')
  .slice(0, -1)
  .join('.');

const normilizerUrl = (path, routerPath) => removeExtension(path.replace(routerPath, ''));

const getFilesFromDirectory = (dirPath, arrayOfFiles) => {
  let files = fs.readdirSync(dirPath);
  
  arrayOfFiles = arrayOfFiles || [];

  files.forEach((file) => {
    const candidadeDirPath = `${dirPath}/${file}`;

    if (fs.statSync(candidadeDirPath).isDirectory()) {
      return getFilesFromDirectory(candidadeDirPath, arrayOfFiles);
    } else {
      arrayOfFiles.push(candidadeDirPath);
    }
  });

  return arrayOfFiles;
}

module.exports = ({ app, config }) => {
  if (app == null) {
    console.log('Express app required');
  }
  
  var routerPath = __dirname + DEFAULT_ROUTER_PATH;
  
  if (config && config.path != null) {
    routerPath = __dirname + config.path;
  }

  const routes = getFilesFromDirectory(routerPath);

  routes.forEach(file => {
    const url = normilizerUrl(file, routerPath);
    const routerFunction = require(file);
    
    if (routerFunction.get) {
      console.log(`GET - ${url}`);
      app.get(url, routerFunction.get);
    }

    if (routerFunction.post) {
      console.log(`POST - ${url}`);

      app.get(url, routerFunction.post);
    }

    if (routerFunction.put) {
      console.log(`PUT - ${url}`);
      app.get(url, routerFunction.put);
    }

    if (routerFunction.delete) {
      console.log(`DELETE - ${url}`);
      app.get(url, routerFunction.delete);
    }
  });
}